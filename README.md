# Azure VM Resize script

## What is?

This script is to resize VM(s) in Azure

## Install

#### Requirements

* Python 3.5+
* pip

### Install the dependencies using pip

```
pip install requirements.txt
```

## Getting Credentials

The script expects the following values to authenticate with Azure
* client_id
* tenant_id
* client_secret

A service principal must be created in the Azure Active Directory where the above values can be retrieved

To have the access to resize a VM, the following things need to be done

* Create a Service principal
* Role assignment

#### Create Service principal

* Click on Azure Active Directory
* On the left menu pane select 'App registrations'
* Click on 'New Registration'
* Create a Service Principal as shown below and click on 'Register'
* Take a copy of the Tenant id and Client id after creating the Service Principal (These are to be used as an argument in resize script)
* Click on 'Certificates & secrets' on the left panel - click on 'New client secret'. Copy the secret key which is client_secret which is used as an argument in the script

#### Role Assignment

Each subscription must be given access to the service principal with the VM Contributor Role to be able to perform resize operation on VMs in the subscription

* Select a subscription
* On the Access control(IAM), click on '+ Add'  and in the drop-down select 'Add role assignment'. 
* Select the Virtual Machine Contributor Role from the Role dropdown
* Select the service principal created in the dropdown to assign the role to SP

After the role is assigned to the subscriptions, keep the client_id, tenant_id, client_secret

## Parameters

  * **--client_id** 
    * Required
    * Client Id of the service principal
    * string in the format - xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
  * **--tenant** 
    * Required
    * Tenant ID of the service principal
    * string in the format - xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx
  * **--secret** 
    * Required
    * Secret key of the service principal
    * string in the format - xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
  * **--vm_name**
    * string
    * Name of the VM
    * must use rg_name and sub_id parameters
  * **--rg_name** 
    * string
    * Resource Group the VM is in
    * must use vm_name and sub_id parameters
  * **--sub_id** 
    * string
    * Subscription id where the Resource group is in
    * must usse vm_name and rg_name parameters
  * **--vm_id** 
    * string
    * complete resource ID of the VM
    * format - /subscriptions/xxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxx/resourcegroups/xxxxxxxxx/providers/microsoft.compute/virtualmachines/xxxxxxx
  * **--restart_vm** 
    * Default: False 
    * using this flag is like giving permission to restart the VM if needed.
    * If the target size is not available in the current hardware cluster, the VM has to be restarted to be resized.
    * Use this flag if VM can be restarted
  * **--retain_public** 
    * Default: False 
    * Using this flag, will make the current public IP attached to VM static.
    * If the VM has to be restarted then using this flag will retain the public IP of the VM
    * Use this flag if the public IP of the VM must be retained after a restart

  * **--retain_public**
    * Default: False 
    * Using this flag, will make the current private IP attached to VM static.
    * If the VM has to be restarted then using this flag will retain the private IP of the VM
    * Use this flag if the private IP of the VM must be retained after a restart

## Usage

The script can be run in three ways

### Using the VM name, ResourceGroup Name, and Subscription ID as arguments
 To resize a single VM to its target size use the below sample command

 ```
 python vm_size_updator.py --client_id "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" --tenant "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx" --secret "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" --vm_name "vm_name" --rg_name "resourcegroup_name" --sub_id "xxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxx" --target_size "Standard_B1s"
 ```

 * This will take less than a minute to complete
 * If the target size is available in the current physical cluster. In such case a reboot is not necessary. 
 * If not available, a message is displayed that size is not available in cluster. A restart is required in such cases to deallocate the VM in the current cluster and allocate in new cluster. So make sure all the process are safe to stop and also make sure to start any process that require manual startup after reboot.
 * A restart would take approximately 30 sec to 2 minute - depends on OS and disk type
 * If public or private IP needs to be retained after reboot use the necessary flags mentioned in the Additional parameters section of the readme,  rention of IP may take 30 seconds to complete

### Using the complete VM resource ID as arguments

 ```
 python vm_size_updator.py --client_id "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" --tenant "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx" --secret "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" --vm_id "/subscriptions/xxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxx/resourcegroups/xxxxxxxxx/providers/microsoft.compute/virtualmachines/xxxxxxx"
 ```

 * This will take less than a minute to complete
 * If the target size is available in the current physical cluster. In such case a reboot is not necessary. 
 * If not available, a message is displayed that size is not available in cluster. A restart is required in such cases to deallocate the VM in the current cluster and allocate in new cluster. So make sure all the process are safe to stop and also make sure to start any process that require manual startup after reboot.
 * A restart would take approximately 30 sec to 2 minute - depends on OS and disk type
 * If public or private IP needs to be retained after reboot use the necessary flags mentioned in the Additional parameters section of the readme,  rention of IP may take 30 seconds to complete

### Using a CSV as input
 The CSV contains VM ids are their respective target sizes to be resized to. This CSV is provided by Leankloud.
 Leankloud will formulate the CSV accoring to the user requirements and approvals, by considering
  * If the target size is suitable for the application 
  * if a restart can be accomodated by the application in the VM
  * if Public IP and private IP needs to be retained

 ```
  python vm_size_updator.py --client_id "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" --tenant "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx" --secret "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" --reco_csv sample-recos.csv
 ```

 * Each VM will take a less than a minute to complete
 * If the VM is in availablility set, then it will resize all the VMs in the availablility set, but other VMs and their target sizes must be in the CSV, else it raises an error (KeyError)
 * The CSV contains the VM, target size, if the VM size is not available in the cluster - restart permission, retention of public and private IP permission

## Log output
 A log file with name "size_update.log" is created after running the script. Attach this log file when requesting support from Leankloud
 Parameters used while running the script are stored in the log file - Keys and credentials are redacted before storing into log file
