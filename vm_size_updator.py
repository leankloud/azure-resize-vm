"""
Copyright [2021] [LeanKloud Solutions Pvt Ltd, Chennai, India]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

# script to resize Azure VM(s)
import argparse
import json
from csv import DictReader
from collections import defaultdict

from azure.identity import ClientSecretCredential
from azure.mgmt.compute import ComputeManagementClient
from azure.mgmt.network import NetworkManagementClient
from azure.mgmt.resource import ResourceManagementClient

from cred_wrapper import AzureIdentityCredentialAdapter

def get_credentials(client_id, secret, tenant):
    """returns Azure ServicePrincipalCredentials object
        for the credentials provided

    Args:
        client_id (str)
        secret (str)
        tenant (str)
    """
    credentials = ClientSecretCredential(
        client_id=client_id,
        client_secret=secret,
        tenant_id=tenant
    )
    return AzureIdentityCredentialAdapter(credentials)

def get_subscription_id_from_vm_id(resource_id):
    """Given a azure resource id, extract the subscription id from it

    Args:
        resource_id (str)

    Returns:
        str: subscription id of the resource
    """
    return resource_id.lstrip("/").split("/")[1]

def get_resource_group_from_id(resource_id):
    """Given a azure resource id, extract the resource group name from it

    Args:
        resource_id (str)

    Returns:
        str: resource group name of the resource
    """
    return resource_id.lstrip("/").split("/")[3]

def get_resource_name_from_id(resource_id):
    """Given a azure resource id, extract the resource name from it

    Args:
        resource_id (str)

    Returns:
        str: resource name
    """
    return resource_id.lstrip("/").split("/")[-1]

class VmResizerBase(object):
    """base class for resizing VM

    Attributes:
        compute_client (ComputeManagementClient)
        network_client (NetworkManagementClient)
        resource_group_client (ResourceManagementClient)
        vms_information (dict): dict of dicts with keys as vm_ids
            and its update requirements and state as values
            This is required to get the required VM resize information for VMs in 
            a availablity set.
            keys:
                sub_id (str): subscription if of the VM
                target_size (str): VM size to be resized to
                restart_permission (bool): permission to restart the VM if the size is unavailable
                retain_public (bool): if public id of the VM must be retained
                retain_private (bool): if public id of the VM must be retained

    """
    def __init__(self):
        self.credentials = self.get_credentials()
        self.vms_information = defaultdict(dict)

    def initialize_clients(self, subscription_id):
        """Iniitalize all the azure management clients for the subscription

        Args:
            subscription_id (str): Subscription ID xxxxxxxx-xxxxx-xxxxx-xxxxx
        """
        # check if the clients are already initialized for the subscription id
        if getattr(self, 'subscription_id', '') == subscription_id:
            return
        self.subscription_id = subscription_id
        logger.info("Initializing clients for subscription ID: {}".format(subscription_id))

        self.resource_group_client = ResourceManagementClient(
            self.credentials,
            subscription_id
        )
        self.network_client = NetworkManagementClient(
            self.credentials,
            subscription_id
        )
        self.compute_client = ComputeManagementClient(
            self.credentials,
            subscription_id
        )

    def store_vm_details(
        self,
        vm_id,
        sub_id,
        retain_public,
        retain_private,
        restart_permission,
        target_size
    ):
        details = self.vms_information[vm_id.lower()]
        details['sub_id'] = sub_id
        details['retain_public'] = retain_public
        details['retain_private'] = retain_private
        details['restart_permission'] = restart_permission
        details['target_size'] = target_size
        

    def get_vm(self, vm_id):
        """Returns Azure VM object

        Args:
            vm_id (str): complete id of the vm in the below format
              "/subscriptions/{}/resourcegroups/{}/providers/microsoft.compute/virtualmachines/{}"

        Returns:
           (azure.mgmt.compute.v2020_06_01.models._models_py3.VirtualMachine): Azure VM object
        """
        rg_name = get_resource_group_from_id(vm_id)
        name = get_resource_name_from_id(vm_id)
        try:
            vm = self.compute_client.virtual_machines.get(
                resource_group_name=rg_name,
                vm_name=name
            )
        except Exception as e:
            logger.exception(
                "Error while getting VM: {} in RG: {}".format(
                    name, rg_name
                )
            )
            raise e
        return vm

    def is_vm_resized(self, vm, target_size):
        if vm.hardware_profile.vm_size.lower() == target_size.lower():
            logger.info("VM - {} size is already {}".format(vm.name, vm_details['target_size']))
            return True
        return False

    def _is_target_size_available(self, vm, target_size):
        rg_name = get_resource_group_from_id(vm.id)
        try:
            available_sizes = self.compute_client.virtual_machines.list_available_sizes(
                rg_name,
                vm.name
            )
        except Exception as e:
            logger.exception(
                "Error while listing available sizes for VM: {} in RG: {}".format(
                    vm.name, rg_name
                )
            )
            raise e
        for size in available_sizes:
            if target_size == size.name:
                return True
        return False

    def belongs_to_availability_set(self, vm):
        """ Returns True if the VM belongs to a availablity set

        Args:
            vm (vm (azure.mgmt.compute.v2020_06_01.models._models_py3.VirtualMachine): Azure VM object

        Returns:
            TYPE: Description
        """
        if vm.availability_set:
            return True
        return False

    def _deallocate_vm(self, vm):
        if 'PowerState/deallocated' in [s.code for s in vm.instance_view.statuses]:
            return
        logger.info("Deallocating the VM {}".format(vm.name))
        rg_name = get_resource_group_from_id(vm.id)
        try:
            async_vm_stop = self.compute_client.virtual_machines.begin_deallocate(
                rg_name, vm.name
            )
            async_vm_stop.wait()
        except Exception as e:
            logger.exception(
                "Error while deallocating VM: {} in RG: {}".format(
                    vm.name, rg_name
                )
            )
            raise e
        

    def _start_vm(self, vm):
        logger.info("Starting the VM {}".format(vm.name))
        rg_name = get_resource_group_from_id(vm.id)
        try:
            async_vm_start = self.compute_client.virtual_machines.begin_start(
                rg_name, vm.name
            )
            async_vm_start.wait()
        except Exception as e:
            logger.exception(
                "Error while starting VM: {} in RG: {}".format(
                    vm.name, rg_name
                )
            )
            raise e
        

    def resize_availability_set(self,
        vm,
        target_size,
        restart_permission=False,
        retain_public=False,
        retain_private=False
    ):
        """Update VM size to the arg:target_size
        Updates all the VMs in the availablity set to its the target size,
        stored in the self.vms_information.
        Currently, resizing availablity set only supports through reco_csv
        https://docs.microsoft.com/en-us/azure/virtual-machines/windows/resize-vm
        #use-powershell-to-resize-a-vm-in-an-availability-set

        Args:
            vm (azure.mgmt.compute.v2020_06_01.models._models_py3.VirtualMachine): Azure VM object
            target_size (str): VM size to which the VM has to be resized
            restart_permission (bool, optional): if true the VM is restarted if the VM size
                is not available in the cluster
            retain_public (bool, optional): if true the public IP of the VM is
                retained in case of a restart
            retain_private (bool, optional): if true the private IP of the VM is
                retained in case of a restart
        """
        avblset_id = vm.availability_set.id
        try:
            avblset = self.resource_group_client.resources.get_by_id(
                avblset_id,
                api_version="2017-12-01"
            )
        except Exception as e:
            logger.exception(
                "Error while getting Available set with ID: {}".format(
                    avblset_id
                )
            )
            raise e
        size_available = self._is_target_size_available(vm, target_size)
        avblset_vms = avblset.properties['virtualMachines']
        vms = [self.get_vm(vm['id']) for vm in avblset_vms]
        restart_perm = all(
            [self.vms_information[_vm.id.lower()]['restart_permission'] for _vm in vms]
        )
        restart = restart_perm and not size_available
        if restart:
            for _vm in vms:
                self._deallocate_vm(_vm)
        for _vm in vms:
            target_size = self.vms_information[_vm.id.lower()]['target_size']
            retain_public = self.vms_information[_vm.id.lower()]['retain_public']
            retain_private = self.vms_information[_vm.id.lower()]['retain_private']
            self.resize_vm(
                _vm,
                target_size,
                restart_permission=restart_perm,
                retain_public=retain_public,
                retain_private=retain_private
            )
            if restart:
                self._start_vm(_vm)

    def _get_nic(self, nic_id):
        rg_name = get_resource_group_from_id(nic_id)
        nic_name = get_resource_name_from_id(nic_id)
        try:
            nic = self.network_client.network_interfaces.get(rg_name, nic_name)
        except Exception as e:
            logger.exception(
                "Error while getting NIC: {} in RG: {} ".format(
                    nic_name, rg_name
                )
            )
            raise e
        return nic

    def _get_public_ip(self, public_ip_id):
        rg_name = get_resource_group_from_id(public_ip_id)
        ip_name = get_resource_name_from_id(public_ip_id)
        ip = self.network_client.public_ip_addresses.get(rg_name, ip_name)
        return ip

    def _retain_ip(self, nic, retain_public, retain_private):
        # reconfigures the network interface controller of the VM
        # to make the public and private IP static
        nic_params = nic.serialize()
        nic_rg_name = get_resource_group_from_id(nic.id)
        nic_name = get_resource_name_from_id(nic.id)
        private_resized = False
        public_resized = False
        # update the configuration of each ip configuration
        for ip_config in nic_params['properties']['ipConfigurations']:
            public_ip_id = ip_config['properties']['publicIPAddress']['id']
            ip = self._get_public_ip(public_ip_id)
            if retain_public and ip.public_ip_allocation_method != 'Static':
                ip_name = get_resource_name_from_id(public_ip_id)
                rg_name = get_resource_group_from_id(public_ip_id)
                ip_params = ip.serialize()
                ip_params['properties']['publicIPAllocationMethod'] = 'Static'
                logger.info("Retaining {} - public IP".format(ip_name))
                try:
                    self.network_client.public_ip_addresses.begin_create_or_update(
                        resource_group_name=rg_name,
                        public_ip_address_name=ip_name,
                        parameters=ip_params
                    )
                except Exception as e:
                    logger.exception(
                        "Error while updating public IP: {} in RG: {}\n with Params: {}".format(
                            ip_name, rg_name, json.dumps(ip_params, indent=2)
                        )
                    )
                    raise e
                public_resized = True
            if retain_private and ip_config['properties']['privateIPAllocationMethod']:
                logger.info("Retaining private IP")
                ip_config['properties']['privateIPAllocationMethod'] = 'Static'
                private_resized = True
        try:
            self.network_client.network_interfaces.begin_create_or_update(
                resource_group_name=nic_rg_name,
                network_interface_name=nic_name,
                parameters=nic_params
            )
        except Exception as e:
            logger.exception(
                "Error while updating the NIC: {} in RG: {}\n with Params: {}".format(
                    nic_name, nic_rg_name, json.dumps(nic_params, indent=2)
                )
            )
            raise e
        if private_resized or public_resized:
            logger.info("IP addresses retained")

    def _update_vmsize(self, vm, target_size):
        if self.is_vm_resized(vm, target_size):
            return True
        vm.hardware_profile.vm_size = target_size
        resource_group_name = get_resource_group_from_id(vm.id)
        logger.info("Resizing VM {} to {}".format(vm.name, target_size))
        try:
            update_op = self.compute_client.virtual_machines.begin_create_or_update(
                resource_group_name,
                vm.name,
                vm.serialize()
            )
        except Exception as e:
            logger.exception(
                "Error while updating the VM - {} to {}".format(vm.name, target_size)
            )
            raise e
        update_op.wait()
        if not update_op.done():
            logger.warning(
                "Update size operation for VM: {} to size: {}\nFailed with result: {}".format(
                    vm.name, target_size, update_op.result()
                )
            )
        return update_op.done()

    def resize_vm(
        self,
        vm,
        target_size,
        restart_permission=False,
        retain_public=False,
        retain_private=False
    ):
        """Update VM size to the arg:target_size

        Args:
            vm (azure.mgmt.compute.v2020_06_01.models._models_py3.VirtualMachine): Azure VM object
            target_size (str): VM size to which the VM has to be resized
            restart_permission (bool, optional): if true the VM is restarted if the VM size
                is not available in the cluster
            retain_public (bool, optional): if true the public IP of the VM is
                retained in case of a restart
            retain_private (bool, optional): if true the private IP of the VM is
                retained in case of a restart
        """
        size_available = self._is_target_size_available(vm, target_size)
        if size_available:
            self._update_vmsize(vm, target_size)
            return True
        logger.info("{} is not available in the current cluster".format(target_size))
        if not restart_permission and not size_available:
            return False
        # check if public and private ip must be retained before stopping the VM
        # must deallocate the VM as opposed to stopping the VM
        # https://jaychapel.medium.com/how-microsoft-azure-deallocate
        # -vm-vs-stop-vm-states-differ-801deb8e5f2a
        if retain_public or retain_private:
            for n_interface in vm.network_profile.network_interfaces:
                nic_id = n_interface.id
                nic = self._get_nic(nic_id)
                self._retain_ip(nic, retain_public=retain_public, retain_private=retain_private)

        if not size_available:
            logger.info("Restarting Vm {}".format(get_resource_name_from_id(vm.id)))
            self._deallocate_vm(vm)
        self._update_vmsize(vm, target_size)
        self._start_vm(vm)
        return True

    def get_credentials(self):
        """ Return azure credentials object
        """
        pass

class CustomerVmResizer(VmResizerBase):
    def __init__(self, client_id, secret, tenant):
        """
        Args:
            client_id (str)
            secret (str)
            tenant (str)
        """
        self.client_id = client_id
        self.secret = secret
        self.tenant = tenant
        super().__init__()

    def get_credentials(self):
        """Returns the ServicePrincipalCredentials object

        Returns:
            ServicePrincipalCredentials
        """
        self.credentials = get_credentials(self.client_id, self.secret, self.tenant)
        return self.credentials


class IllegalArgumentError(ValueError):

    """Exception when the cli arguments are illegal
    """
    pass

def redact_sensitive_args(args, sensitive_args):
    """
    Returns a dict with keys as argument names and its values
    Redacts the sesitive information in the arguments

    Args:
        args (argparse.Namespace): arg parser with arguments parsed
        sensitive_args (list): names of the arguments with sensitive information
    
    Returns:
        (dict): keys are argument names and redacted values of the arguments
    
    """
    r_args = {}
    for arg_n, arg_v in args._get_kwargs():
        if arg_n in sensitive_args:
            v = "REDACTED"
        else:
            v = arg_v
        r_args[arg_n] = v
    return r_args

if __name__ == '__main__':
    import logging

    # create logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    stream_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    ch.setFormatter(stream_formatter)

    fh = logging.FileHandler('size_update.log')
    fh.setLevel(logging.INFO)
    file_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(file_formatter)

    logger.addHandler(ch)
    logger.addHandler(fh)

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument(
        '-c', '--client_id',
        required=True, 
        help='client id of the azure account'
    )
    args_parser.add_argument('-t', '--tenant', required=True, help='tenant id of the azure account')
    args_parser.add_argument('-s', '--secret', required=True, help='secret for access to azure')
    args_parser.add_argument(
        '--restart_vm',
        default=False,
        action='store_true',
        help=(
            'restart id necessary for the vm inorder to resize to a size'
            ' which is not available in the cluster, using this flag will restart'
            ' the vm and resize the vm to target size. By default it will raise'
            ' a error message for the vm that resizeing without restart is not possible'
        )
    )
    args_parser.add_argument(
        '--retain_public',
        default=False,
        action='store_true',
        help=(
            'restarting the ip will not retain the IP configuration, use this flag to retain the'
            ' public IP in primary network interface'
        )
    )
    args_parser.add_argument(
        '--retain_private',
        default=False,
        action='store_true',
        help=(
            'restarting the ip will not retain the IP configuration, use this flag to retain the'
            ' private IP in primary network interface'
        )
    )
    args_parser.add_argument('-v', '--vm_name', help='name of the VM')
    args_parser.add_argument('-r', '--rg_name', help='Resource group name of the VM')
    args_parser.add_argument('--sub_id', help='Subscription id in the azure account of the VM')
    args_parser.add_argument('--vm_id', help='complete resource id of the VM')
    args_parser.add_argument('--target_size', help='vm size to which vm has to be resized')
    args_parser.add_argument('--reco_csv', help='csv path of the recommendations')

    args = args_parser.parse_args()
    logger.info(
        "Arguments: " + json.dumps(
            redact_sensitive_args(args, ['client_id', 'tenant', 'secret']),
            indent=2
        )
    )
    try:
        if not (args.vm_id or args.vm_name or args.reco_csv):
            raise IllegalArgumentError("either vm_id, vm_name, or reco_csv must be passed")
        if args.vm_name and ( not args.rg_name or not args.sub_id):
            raise IllegalArgumentError("when vm_name along with rg_name and sub_id must be passed")
    except Exception as e:
        logger.exception("Illegal argument")
        raise e

    vm_size_updator = CustomerVmResizer(args.client_id, args.secret,  args.tenant)
    if args.vm_name:
        vm_id = (
            "/subscriptions/{}/resourcegroups/{}/providers/microsoft.compute/virtualmachines/{}"
        ).format(
            args.sub_id,
            args.rg_name,
            args.vm_name
        )
    elif args.vm_id:
        vm_id = args.vm_id
    if args.reco_csv:
        try:
            with open(args.reco_csv, 'r') as reco_f:
                recos = DictReader(reco_f)
                for reco in recos:
                    # store the details of the VM that needs to be resized
                    vm_id = reco['vm_id']
                    public = reco['retain public ip'].lower() == 'true'
                    private = reco['retain private ip'].lower() == 'true'
                    restart = reco['restart vm'].lower() == 'true'
                    vm_size_updator.store_vm_details(
                        vm_id=vm_id,
                        sub_id=get_subscription_id_from_vm_id(vm_id),
                        target_size=reco['target size'],
                        retain_public=public,
                        retain_private=private,
                        restart_permission=restart
                    )
        except Exception as e:
            logger.exception("Error with the recommendations CSV file")
            raise e
    else:
        # store the details of the VM that needs to be resized
        vm_size_updator.store_vm_details(
            vm_id=vm_id,
            sub_id=get_subscription_id_from_vm_id(vm_id),
            retain_public=args.retain_public,
            retain_private=args.retain_private,
            restart_permission=args.restart_vm,
            target_size=args.target_size,
        )

    for vm_id, vm_details in vm_size_updator.vms_information.items():
        # initialize management clients for the subscription
        vm_size_updator.initialize_clients(vm_details['sub_id'])
        vm = vm_size_updator.get_vm(vm_id=vm_id)
        vm_size = vm.hardware_profile.vm_size
        if vm_size_updator.is_vm_resized(vm, vm_details['target_size']):
            continue
        if vm_size_updator.belongs_to_availability_set(vm):
            logger.info("VM - {} is in an availability_set".format(vm.name))
            if args.rg_name:
                try:
                    raise IllegalArgumentError((
                        "if the VM is in availablity set, pass a recommendations",
                        " CSV with target sizes of all the VMs in the availablity set"
                    ))
                except Exception as e:
                    logger.exception("Vm is part of availability set")
                    raise e

            vm_size_updator.resize_availability_set(
                vm,
                vm_details['target_size'],
                restart_permission=vm_details['restart_permission'],
                retain_public=vm_details['retain_public'],
                retain_private=vm_details['retain_private']
            )
        else:
            vm_size_updator.resize_vm(
                vm,
                vm_details['target_size'],
                restart_permission=vm_details['restart_permission'],
                retain_public=vm_details['retain_public'],
                retain_private=vm_details['retain_private']
            )
